<?php

namespace App\Service;

use App\Repository\CompanyRepository;
use App\Dto\Response\Transformer\CompanyDtoTransformer;
use App\Dto\Response\Transformer\CompanyListDtoTransformer;

class CompanyService
{
    public function __construct(
        private readonly CompanyRepository $repository,
    ) {
    }

    public function getCompanyById(int $id): object
    {
        $company = $this->repository->find($id);
        return (new CompanyDtoTransformer)->transformFromObject($company);
    }

    public function getPaginatedCompanies(int $offset, int $limit): array
    {
        $companies = $this->repository->findAllWithPagination($offset, $limit);
        return (new CompanyListDtoTransformer)->transformFromObjects($companies);
    }
}
