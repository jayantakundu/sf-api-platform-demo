<?php

declare(strict_types=1);

namespace App\Dto\Response;

use Symfony\Component\Validator\Constraints as Assert;

class CompanyListDto
{
    #[Assert\NotBlank]
    public int $id;

    #[Assert\NotBlank]
    public string $name;

    #[Assert\NotBlank]
    public string $desc;
}
