<?php

declare(strict_types=1);

namespace App\Dto\Response;

use Symfony\Component\Validator\Constraints as Assert;

class CompanyDto
{
    #[Assert\NotBlank]
    public int $id;

    #[Assert\NotBlank]
    public string $name;
}
