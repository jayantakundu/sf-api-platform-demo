<?php

declare(strict_types=1);

namespace App\Dto\Response\Transformer;

use App\Dto\Exception\UnexpectedTypeException;
use App\Dto\Response\CompanyListDto;
use App\Entity\Company;

class CompanyListDtoTransformer extends AbstractResponseDtoTransformer
{
    /**
     * @param Company $company
     *
     * @return CompanyListDto
     */
    public function transformFromObject($company): CompanyListDto
    {
        if (!$company instanceof Company) {
            throw new UnexpectedTypeException('Expected type of Company but got ' . \get_class($company));
        }

        $dto = new CompanyListDto();
        $dto->id = $company->getId();
        $dto->name = $company->getName();
        $dto->desc = 'abc';

        return $dto;
    }
}
