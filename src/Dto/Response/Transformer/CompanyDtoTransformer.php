<?php

declare(strict_types=1);

namespace App\Dto\Response\Transformer;

use App\Dto\Exception\UnexpectedTypeException;
use App\Dto\Response\CompanyDto;
use App\Entity\Company;

class CompanyDtoTransformer extends AbstractResponseDtoTransformer
{
    /**
     * @param Company $company
     *
     * @return CompanyDto
     */
    public function transformFromObject($company): CompanyDto
    {
        if (!$company instanceof Company) {
            throw new UnexpectedTypeException('Expected type of Company but got ' . \get_class($company));
        }

        $dto = new CompanyDto();
        $dto->id = $company->getId();
        $dto->name = $company->getName();

        return $dto;
    }
}
