<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Service\CompanyService;

class GetCompanyProvider implements ProviderInterface
{
    public function __construct(
        private readonly CompanyService $service,
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if ('get_company' !== ($context['operation_name'] ?? '')) {
            return null;
        }
        $id = (int)$uriVariables['id'];
        // Retrieve the state from somewhere
        return $this->service->getCompanyById($id);
    }

}
