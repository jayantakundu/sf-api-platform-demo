<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Service\CompanyService;

class CompanyProvider implements ProviderInterface
{
    public function __construct(
        private readonly CompanyService $service,
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        switch ($operation->getName()) {
            case "get_companies":
                $response = $this->getPaginatedCompanies($uriVariables, $context);
                break;
            case "get_company":
                $response = $this->getCompanyById($uriVariables, $context);
                break;
            default:
                $response = null;
        }
        return $response;
    }

    private function getPaginatedCompanies($uriVariables, $context)
    {
        $limit = 20;
        $page = (int) $context['filters']['page'];
        $offset = (int) ($page <= 1) ? 0 : ($page -1) * $limit;

        return $this->service->getPaginatedCompanies($offset, $limit);
    }

    private function getCompanyById($uriVariables, $context)
    {
        $id = (int)$uriVariables['id'];
        return $this->service->getCompanyById($id);
    }
}
