# Base Image
FROM --platform=linux/amd64 php:8.2-fpm
ENV ACCEPT_EULA=Y

# INSTALL PHP Extensions
RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    gnupg2 \
    libzip-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    unzip \
    freetds-dev \
    freetds-bin \
    tdsodbc \
    unixodbc \
    unixodbc-dev \
    libsybdb5 \
    libicu-dev \
    libxml2-dev \
    librabbitmq-dev \
    libssl-dev \
    && ln -s /usr/lib/x86_64-linux-gnu/libsybdb.so /usr/lib/libsybdb.so

# MSSQL support
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - 
RUN curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update && ACCEPT_EULA=Y apt-get -y --no-install-recommends install msodbcsql17 

RUN docker-php-ext-configure gd && \
    docker-php-ext-install gd && \
    docker-php-ext-configure zip && \
    docker-php-ext-install zip && \
    docker-php-ext-install intl && \
    docker-php-ext-enable opcache && \
    docker-php-ext-install bcmath && \
    docker-php-ext-install soap && \
    docker-php-ext-install sockets && \
    docker-php-ext-install pdo && \
    docker-php-ext-install pdo_dblib && \
    docker-php-ext-enable pdo_dblib && \
    docker-php-ext-install pdo_mysql

# INSTALL other Extensions
RUN apt-get update && pecl install apcu \
    && docker-php-ext-enable apcu \
    && pecl install redis \
    && docker-php-ext-enable redis \
	&& pecl install sqlsrv \
	&& docker-php-ext-enable sqlsrv \
	&& pecl install pdo_sqlsrv \
	&& docker-php-ext-enable pdo_sqlsrv \
    && pecl install amqp \
    && docker-php-ext-enable amqp

# copying the source directory
RUN mkdir -p /var/www/html

COPY . /var/www/html
WORKDIR /var/www/html

# INSTALL COMPOSER
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -sS https://get.symfony.com/cli/installer | bash && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony

# run composer install to install the dependencies
 RUN composer install \
  --optimize-autoloader \
  --no-interaction

 # Set Permissions
RUN chmod -R 0777 /var/www/html/var

# set Configuration
# COPY ./docker/etc/php/php.ini /usr/local/etc/php/php.ini

# Expose the port
EXPOSE 8000

# Let start the app
CMD ["symfony", "server:start"]
# CMD ["symfony","serve", "-d"]
